import {
    GET_ARRIVAL_FLIGHT_FAILED,
    GET_ARRIVAL_FLIGHT_PENDING,
    GET_ARRIVAL_FLIGHTS_SUCCESS,
    GET_DASHBOARD_DATA,
    GET_DEPARTURE_FLIGHT_FAILED,
    GET_DEPARTURE_FLIGHT_PENDING,
    GET_DEPARTURE_FLIGHTS_SUCCESS, RESET_STATE,
} from "../actions/actionsTypes";
//https://opensky-network.org/api/flights/arrival?airport=KLGA&begin=1517227200&end=1517230800
//https://opensky-network.org/api/flights/departure?airport=KLGA&begin=1517227200&end=1517230800

let initialDashboardState = {
    cities: [
        {
            city: 'London',
            airport: 'Gatwick Airport',
            code: 'EGKK'
        },
        {
            city: 'New York',
            airport: 'LaGuardia Airport',
            code: 'KLGA'
        },
        {
            city: 'Los Angeles',
            airport: 'Los Angeles International Airport',
            code: 'KLAX'
        },
        {
            city: 'Shanghai',
            airport: 'Shanghai Pudong International Airport',
            code: 'ZSPD'
        },
        {
            city: 'Tokyo',
            airport: 'Haneda Airport',
            code: 'RJTT'
        },
        {
            city: 'Amsterdam',
            airport: 'Amsterdam Airport Schiphol',
            code: 'EHAM'
        },
        {
            city: 'Paris',
            airport: 'Charles de Gaulle Airport',
            code: 'LFPG'
        },
        {
            city: 'Atlanta',
            airport: 'Hartsfield–Jackson Atlanta International Airport',
            code: 'KATL'
        },
        {
            city: 'Dubai',
            airport: 'Dubai International Airport',
            code: 'OMDB'
        },
        {
            city: 'Miami',
            airport: 'Miami International Airport',
            code: 'KMIA'
        }],
};

const dashboardReducer = (state = initialDashboardState, action) => {
    switch (action.type) {
        case GET_DASHBOARD_DATA:
            return state.cities;
        default:
            return state;
    }
};

let initialArrivalState = {
    flightsArrival: [],
    arrivalRequestPending: false,
    arrivalRequestFailed: false,
    arrivalRequestSuccess: false,
};

const arrivalFlightsReducer = (state = initialArrivalState, action) => {
    switch (action.type) {
        case GET_ARRIVAL_FLIGHTS_SUCCESS:
            return {
                ...state,
                flightsArrival: action.payload,
                arrivalRequestPending: false,
                arrivalRequestFailed: false,
                arrivalRequestSuccess: true
            };
        case GET_ARRIVAL_FLIGHT_FAILED:
            return {
                ...state,
                arrivalRequestPending: false,
                arrivalRequestFailed: true,
                arrivalRequestSuccess: false
            };
        case GET_ARRIVAL_FLIGHT_PENDING:
            return {
                ...state,
                arrivalRequestPending: true,
            };
        case RESET_STATE:
            return {
                flightsArrival: [],
                arrivalRequestPending: false,
                arrivalRequestFailed: false,
                arrivalRequestSuccess: false,
            };
        default:
            return state;
    }
};

let initialDepartureState = {
    flightsDeparture: [],
    departureRequestPending: false,
    departureRequestFailed: false,
    departureRequestSuccess: false,
};
const departureFlightsReducer = (state = initialDepartureState, action) => {
    switch (action.type) {
        case GET_DEPARTURE_FLIGHTS_SUCCESS:
            return {
                ...state,
                flightsDeparture: action.payload,
                departureRequestPending: false,
                departureRequestFailed: false,
                departureRequestSuccess: true
            };
        case GET_DEPARTURE_FLIGHT_FAILED:
            return {
                ...state,
                departureRequestPending: false,
                departureRequestFailed: true,
                departureRequestSuccess: false
            };
        case GET_DEPARTURE_FLIGHT_PENDING:
            return {
                ...state,
                departureRequestPending: true,
            };
        case RESET_STATE:
            return {
                flightsDeparture: [],
                departureRequestPending: false,
                departureRequestFailed: false,
                departureRequestSuccess: false,
            };
        default:
            return state;
    }
};

export {dashboardReducer, arrivalFlightsReducer, departureFlightsReducer};
