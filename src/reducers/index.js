import {loginReducer} from "./loginReducer";
import {combineReducers} from "redux";
import {arrivalFlightsReducer, dashboardReducer, departureFlightsReducer} from "./dashboardReducer";

const rootReducer = combineReducers({
    login: loginReducer,
    dashboard: dashboardReducer,
    departure: departureFlightsReducer,
    arrival: arrivalFlightsReducer,
});

export default rootReducer;