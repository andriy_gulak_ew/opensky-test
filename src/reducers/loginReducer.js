import {LOGIN_USER_FAILURE, LOGIN_USER_REQUEST_START, LOGIN_USER_SUCCESS, LOGOUT_USER,} from "../actions/actionsTypes";

let initialLoginState = {
    isLoginRequestFailed: false,
    isLoginRequestPending: false,
    isLoggedIn: false
};

const loginReducer = (state = initialLoginState, action) => {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                isLoginRequestFailed: false,
                isLoginRequestPending: false,
                isLoggedIn: true
            };
        case LOGOUT_USER:
            return {
                ...state,
                isLoginRequestFailed: false,
                isLoginRequestPending: false,
                isLoggedIn: false
            };
        case LOGIN_USER_FAILURE:
            return {
                ...state,
                isLoginRequestFailed: true,
                isLoginRequestPending: false,
                isLoggedIn: false
            };
        case LOGIN_USER_REQUEST_START:
            return {...state, isLoginRequestPending: true};
        default:
            return state;
    }
};

export {loginReducer};
