import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Provider} from "react-redux";
import configureStore from "./store/configureStore";
import {getItem, IS_USER_LOGGED_IN} from "./utils/localStorageUtils";
import {loginUserSuccess} from "./actions/loginActions";

export const store = configureStore();

let isUserLoggedIn = getItem(IS_USER_LOGGED_IN);
if (isUserLoggedIn) {
    store.dispatch(loginUserSuccess());
}

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);
