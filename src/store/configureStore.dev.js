import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import reduxImmutableStateInvariant from "redux-immutable-state-invariant";
import {createLogger} from "redux-logger";
import rootReducer from "../reducers";
import {composeWithDevTools} from "redux-devtools-extension";

const configureStore = preloadedState => {
    const store = createStore(
        rootReducer,
        preloadedState,
        composeWithDevTools(
            applyMiddleware(thunk, createLogger(), reduxImmutableStateInvariant())
        ));

    return store
};

export default configureStore;
