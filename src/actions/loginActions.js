import {
    LOGIN_USER_FAILURE,
    LOGIN_USER_REQUEST_FINISH,
    LOGIN_USER_REQUEST_START,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER
} from "./actionsTypes";

export const loginUserSuccess = () => ({
    type: LOGIN_USER_SUCCESS,
});

export const userLogout = () => ({
    type: LOGOUT_USER,
});

const loginUserFailure = () => ({
    type: LOGIN_USER_FAILURE
});

export const loginUserRequestStart = () => ({
    type: LOGIN_USER_REQUEST_START
});

const loginUserRequestFinish = () => ({
    type: LOGIN_USER_REQUEST_FINISH
});


export function loginUser(login, password) {

}