import {checkStatus, getHttpClient} from "../utils/networkUtils";
import {ARRIVAL_FLIGHTS, DEPARTURE_FLIGHTS} from "../environment";
import {
    GET_ARRIVAL_FLIGHT_FAILED,
    GET_ARRIVAL_FLIGHT_PENDING,
    GET_ARRIVAL_FLIGHTS_SUCCESS,
    GET_DEPARTURE_FLIGHT_FAILED,
    GET_DEPARTURE_FLIGHT_PENDING,
    GET_DEPARTURE_FLIGHTS_SUCCESS, RESET_STATE
} from "./actionsTypes";

export const resetState = () => ({
    type: RESET_STATE
});

const getArrivalFlightsFailure = () => ({
    type: GET_ARRIVAL_FLIGHT_FAILED
});

export const getArrivalFlightsStart = () => ({
    type: GET_ARRIVAL_FLIGHT_PENDING
});


export const getArrivalFlightsSuccess = (data) => ({
    type: GET_ARRIVAL_FLIGHTS_SUCCESS,
    payload: data
});

function getArrivalFlights(airport, begin, end) {
    const url = ARRIVAL_FLIGHTS;
    let options = {
        params: {
            'airport': airport,
            'begin': begin,
            'end': end
        }
    };
    const processError = (error) => {
        console.log(error)
    };

    const request = getHttpClient().get(url, options);
    return (dispatch) => {
        request
            .then(response => {
                return checkStatus(response);
            })
            .then(({data}) => {
                if (data) {
                    return dispatch(getArrivalFlightsSuccess(data));
                }
                else {
                    return dispatch(getArrivalFlightsFailure())
                }
            })
            .catch(error => {
                processError(error);
            });
    }
}


const getDepartureFlightsFailure = () => ({
    type: GET_DEPARTURE_FLIGHT_FAILED
});

export const getDepartureFlightsStart = () => ({
    type: GET_DEPARTURE_FLIGHT_PENDING
});


export const getDepartureFlightsSuccess = (data) => ({
    type: GET_DEPARTURE_FLIGHTS_SUCCESS,
    payload: data
});

function getDepartureFlights(airport, begin, end) {
    const url = DEPARTURE_FLIGHTS;
    let options = {
        params: {
            'airport': airport,
            'begin': begin,
            'end': end
        }
    };
    const processError = (error) => {
        console.log(error)
    };

    const request = getHttpClient().get(url, options);
    return (dispatch) => {
        request
            .then(response => {
                return checkStatus(response);
            })
            .then(({data}) => {
                if (data) {
                    return dispatch(getDepartureFlightsSuccess(data));
                }
                else {
                    return dispatch(getDepartureFlightsFailure())
                }
            })
            .catch(error => {
                processError(error);
            });
    }
}

export {getArrivalFlights, getDepartureFlights};