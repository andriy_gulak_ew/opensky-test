import React from 'react';
import {Card, CardBody, CardHeader, Col, Row} from 'reactstrap';
import AuthForm from "./AuthForm";

export const LoginPage = () => (
    <Row className="login">
        <Col md={{size: 4, offset: 4}} sm="12" xs="12">
            <Card>
                <CardHeader>Please login to view flights</CardHeader>
                <CardBody>
                    <AuthForm/>
                </CardBody>
            </Card>
        </Col>
    </Row>
);