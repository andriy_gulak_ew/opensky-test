import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from 'reactstrap';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {loginUserSuccess} from "../actions/loginActions";
import {Redirect} from "react-router-dom";
import {IS_USER_LOGGED_IN, setItem} from "../utils/localStorageUtils";

class AuthForm extends Component {
    state = {
        loginInput: '',
        passwordInput: '',
        isLoggedIn: false,
        isPasswordInputInvalid: false,
        isLoginInputInvalid: false
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.isLoggedIn !== nextProps.isLoggedIn) {
            return {
                isLoggedIn: nextProps.isLoggedIn
            }
        } else {
            return null;
        }
    }

    handleSubmit = e => {
        const {loginInput, passwordInput} = this.state;

        if ((loginInput && loginInput === 'demo') && (passwordInput && passwordInput === 'demo')) {
            this.props.loginActions.loginUserSuccess();
            setItem(IS_USER_LOGGED_IN, true);
            this.setState({isPasswordInputInvalid: false, isLoginInputInvalid: false});
        } else {
            this.setState({isPasswordInputInvalid: true, isLoginInputInvalid: true});
        }
    };

    onChange = e => {
        this.setState({[e.target.name]: e.target.value})
    };

    render() {
        console.log('state',this.state)
        if (this.state.isLoggedIn) {
            return <Redirect to="/dashboard"/>
        }
        const {
            usernameLabel,
            usernameInputProps,
            passwordLabel,
            passwordInputProps,
        } = this.props;

        const {isPasswordInputInvalid, isLoginInputInvalid} = this.state;
        return (
            <Form onSubmit={this.handleSubmit}>
                <div className="text-center pb-4">
                    <img
                        src="https://reduction-admin.github.io/react-reduction/static/media/logo_200.b175e1c4.png"
                        className="rounded"
                        style={{width: 60, height: 60, cursor: 'pointer'}}
                        alt="logo"
                    />
                </div>
                <FormGroup>
                    <Label for={usernameLabel}>{usernameLabel}</Label>
                    <Input invalid={isLoginInputInvalid} name="loginInput"
                           onChange={this.onChange} {...usernameInputProps} />
                </FormGroup>
                <FormGroup>
                    <Label for={passwordLabel}>{passwordLabel}</Label>
                    <Input invalid={isPasswordInputInvalid} name="passwordInput"
                           onChange={this.onChange} {...passwordInputProps} />
                </FormGroup>
                <hr/>
                <Button
                    size="lg"
                    className="bg-gradient-theme-left border-0"
                    block
                    onClick={this.handleSubmit}>
                    Login
                </Button>
            </Form>
        );
    }
}

export const STATE_LOGIN = 'LOGIN';

AuthForm.propTypes = {
    authState: PropTypes.oneOf([STATE_LOGIN]).isRequired,
    usernameLabel: PropTypes.string,
    usernameInputProps: PropTypes.object,
    passwordLabel: PropTypes.string,
    passwordInputProps: PropTypes.object,
    confirmPasswordLabel: PropTypes.string,
    confirmPasswordInputProps: PropTypes.object,
};

AuthForm.defaultProps = {
    authState: 'LOGIN',
    usernameInputProps: {
        type: 'text',
        placeholder: 'your login',
    },
    passwordLabel: 'Password',
    passwordInputProps: {
        type: 'password',
        placeholder: 'your password',
    },
    confirmPasswordInputProps: {
        type: 'password',
        placeholder: 'confirm your password',
    },
};

function mapStateToProps(state, ownProps) {
    return {
        isLoggedIn: state.login.isLoggedIn,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loginActions: bindActionCreators({loginUserSuccess}, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthForm);
