import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    Button,
    Card,
    CardText,
    CardTitle,
    Col, FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Modal,
    ModalBody,
    ModalFooter,
    Row
} from "reactstrap";
import {bindActionCreators} from "redux";
import {
    getArrivalFlights,
    getArrivalFlightsStart,
    getDepartureFlights,
    getDepartureFlightsStart,
    resetState
} from "../actions/dashboardActions";
import TableComponent from "./TableComponent";
import TabsComponent from "./TabsComponent";
import {getBeforeValueInUnixTimeStamp, getUnixTimeStampTime, SEVEN_DAYS_IN_MINUTES} from "../utils/networkUtils";


const GridItem = ({city, airport, code, onClickAction}) => (
    <Col className="grid-item" md="3" sm="12" xs="12">
        <Card body>
            <CardTitle>{city}</CardTitle>
            <CardText>{airport}</CardText>
            <Button onClick={() => onClickAction(code)}>Show modal</Button>
        </Card>
    </Col>
);

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cities: props.cities,
            departureFlights: [],
            arrivalFlights: [],
            code: 0,
            isModalOpen: false,
            departingTime: +new Date(),
            arrivalTime: +new Date(),
            isDepartingTimeInvalid: false,
            isArrivalTimeInvalid: false,
            isLoggedIn: true
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.departureFlights !== nextProps.departure) {
            return {
                departureFlights: nextProps.departure
            }
        } else if (prevState.arrivalFlights !== nextProps.arrival) {
            return {
                arrivalFlights: nextProps.arrival
            }
        } else if (prevState.isLoggedIn !== nextProps.isLoggedIn) {
            return {
                isLoggedIn: nextProps.isLoggedIn
            }
        } else {
            return null;
        }
    }

    toggleModal = code => {
        this.setState({
            isModalOpen: !this.state.isModalOpen,
            code: code
        });
    };

    onChange = (e) => {
        this.setState({[e.target.name]: +e.target.value});
    };

    renderModal = () => {
        const {isModalOpen, departureFlights, arrivalFlights, code, departingTime,
            arrivalTime, isDepartingTimeInvalid, isArrivalTimeInvalid} = this.state;
        console.log('renderModal', code);
        return (
            <Modal
                isOpen={isModalOpen}
                toggle={this.toggleModal}
                size="xs"
                backdrop="static"
                backdropClassName="modal-backdrop-light"
                centered>

                <ModalBody>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText>Departing flights in the last</InputGroupText>
                        </InputGroupAddon>
                        <Input invalid={isDepartingTimeInvalid} type="number" name="departingTime" placeholder="15..." onChange={this.onChange}/>
                        <InputGroupAddon addonType="append">
                            <InputGroupText>minutes</InputGroupText>
                        </InputGroupAddon>
                        <Button color="success" onClick={() => {
                            if (!departingTime || departingTime > SEVEN_DAYS_IN_MINUTES || departingTime < 0) {
                                this.setState({isDepartingTimeInvalid: true})
                            } else {
                                this.setState({isDepartingTimeInvalid: false});
                                this.props.dashboardActions.getDepartureFlightsStart();
                                this.props.dashboardActions.getDepartureFlights(code, getBeforeValueInUnixTimeStamp(departingTime), getUnixTimeStampTime())
                            }
                        }}>Find</Button>
                    </InputGroup>

                    <hr/>

                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText>Arriving flights in the last</InputGroupText>
                        </InputGroupAddon>
                        <Input invalid={isArrivalTimeInvalid} type="number" name="arrivalTime" placeholder="15..." onChange={this.onChange}/>
                        <InputGroupAddon addonType="append">
                            <InputGroupText>minutes</InputGroupText>
                        </InputGroupAddon>
                        <Button color="success" onClick={() => {
                            if (!arrivalTime || arrivalTime > SEVEN_DAYS_IN_MINUTES || arrivalTime < 0) {
                                this.setState({isArrivalTimeInvalid: true})
                            } else {
                                this.setState({isArrivalTimeInvalid: false});
                                this.props.dashboardActions.getArrivalFlightsStart();
                                this.props.dashboardActions.getArrivalFlights(code, getBeforeValueInUnixTimeStamp(arrivalTime), getUnixTimeStampTime())
                            }
                        }}>Find</Button>
                    </InputGroup>

                    <TabsComponent
                        departureComponent={
                            <TableComponent flightsData={departureFlights}/>
                        }
                        arrivalComponent={
                            <TableComponent flightsData={arrivalFlights}/>
                        }
                    >
                    </TabsComponent>
                </ModalBody>
                <ModalFooter>
                    <Button color="danger" onClick={() => {
                        this.toggleModal();
                        this.props.dashboardActions.resetState()
                    }}>Close</Button>
                </ModalFooter>
            </Modal>
        );
    };

    render() {
        console.log('state', this.state)
        const {cities} = this.state;

        const citiesBlock = cities.map(data => {
            return (
                <GridItem key={data.airport}
                          city={data.city}
                          airport={data.airport}
                          code={data.code}
                          onClickAction={code => {
                              this.toggleModal(code);
                          }}
                />
            )
        });
        return (
            <Row className="grid-view">
                {citiesBlock}
                {this.renderModal()}
            </Row>
        )
    }
}

function mapStateToProps(state, ownProps) {
    let departureData = state.departure.flightsDeparture;
    departureData = departureData.length > 10 ? departureData.slice(0, 10) : departureData;

    let arrivalData = state.arrival.flightsArrival;
    arrivalData = arrivalData.length > 10 ? arrivalData.slice(0, 10) : arrivalData;

    return {
        cities: state.dashboard.cities,
        departure: departureData,
        arrival: arrivalData,
        isLoggedIn: state.login.isLoggedIn,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dashboardActions: bindActionCreators({
            getArrivalFlightsStart,
            getArrivalFlights,
            getDepartureFlightsStart,
            getDepartureFlights,
            resetState
        }, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
