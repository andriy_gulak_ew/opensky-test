import React from 'react';
import {Table} from 'reactstrap';

const TableRow = ({icao, depAirport, arAirport, callsign}) => (
    <tr key={icao + callsign}>
        <th scope="row">{icao}</th>
        <td>{callsign}</td>
        <td>{depAirport}</td>
        <td>{arAirport}</td>
    </tr>
);

const TableComponent = ({flightsData}) => {
    let mappedFlights = flightsData.map((data) => {
        return (
            <TableRow
                      icao={data.icao24}
                      depAirport={data.estDepartureAirport}
                      arAirport={data.estArrivalAirport}
                      callsign={data.callsign}/>
        )
    });
    return (
        <Table striped>
            <thead>
            <tr>
                <th>ICAO 24 Number</th>
                <th>Call Sign</th>
                <th>Departure Airport</th>
                <th>Arrival Airport</th>
            </tr>
            </thead>
            <tbody>
            {mappedFlights}
            </tbody>
        </Table>
    );
};

export default TableComponent;