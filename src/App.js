import React, {Component} from 'react';
import {Nav, Navbar, NavbarBrand, NavItem, NavLink} from 'reactstrap';
import {LoginPage} from "./components/LoginPage";
import Dashboard from "./components/Dashboard";
import {BrowserRouter, Link, Redirect, Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {userLogout} from "./actions/loginActions";
import {IS_USER_LOGGED_IN, setItem} from "./utils/localStorageUtils";

const PrivateRoute = ({component: Component, isLoggedIn = false, ...rest}) => (
    <Route {...rest} render={(props) => (
        isLoggedIn
            ? <Component {...props} />
            : <Redirect to='/login'/>
    )}/>
);

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {isLoggedIn} = this.props;
        return (
            <BrowserRouter>
                <div>
                    <Navbar color="light" light expand="md">
                        <NavbarBrand tag={Link} to="/">OpenSky</NavbarBrand>
                        <Nav className="ml-auto" navbar>
                            {!isLoggedIn
                                ? <NavItem>
                                    <NavLink tag={Link} to="/login">Login</NavLink>{" "}
                                </NavItem>
                                :
                                <NavItem>
                                    <NavLink onClick={() => {
                                        this.props.loginActions.userLogout();
                                        setItem(IS_USER_LOGGED_IN, '')
                                    }}>Logout</NavLink>{" "}
                                </NavItem>
                            }
                            <NavItem>
                                <NavLink tag={Link} to="/dashboard">Dashboard</NavLink>
                            </NavItem>
                        </Nav>
                    </Navbar>
                    <Switch>
                        <PrivateRoute exact path="/" isLoggedIn={isLoggedIn} component={Dashboard}/>
                        <Route path="/login" component={LoginPage}/>
                        <PrivateRoute path="/dashboard" isLoggedIn={isLoggedIn} component={Dashboard}/>
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        isLoggedIn: state.login.isLoggedIn,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loginActions: bindActionCreators({userLogout}, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);