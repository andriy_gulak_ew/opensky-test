import axios from "axios";

export const SEVEN_DAYS_IN_MINUTES = 7 * 24 * 60;

export const getBeforeValueInUnixTimeStamp = (minutes) => {
    let seconds = minutes & 60;
    let number = +new Date() - seconds * 1000 * 60;
    return Math.floor(number / 1000)
};

export const getUnixTimeStampTime = () => {
    let number = +new Date();
    return Math.floor(number / 1000)
};

var instance = axios.create();

// Override timeout default for the library
// All requests will wait 20 seconds before timing out
const DEFAULT_TIMEOUT = 20 * 1000;

instance.defaults.timeout = DEFAULT_TIMEOUT;

export const getHttpClient = () => {
    return instance;
};

export const checkStatus = (response) => {
    let checkResponseStatus = response.status >= 200 && response.status < 300;

    if (checkResponseStatus) {
        return response;
    } else if (!checkResponseStatus) {
        return null;
    } else {
        let error = new Error('Error');
        if (response.data && response.data.message) {
            console.log('response.data', response.data);
            error = new Error('Error: ' + response.data.message);
        }
        if (process.env.NODE_ENV !== 'production') {
            console.log(error);
        }
        throw error;
    }
};
