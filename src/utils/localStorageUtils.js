export const IS_USER_LOGGED_IN = "IS_USER_LOGGED_IN";


export const getItem = (key) => {
    try {
        return localStorage.getItem(key);
    } catch (err) {
        console.log(err);
        return '';
    }
};
export const setItem = (key, value) => {
    try {
        localStorage.setItem(key, value);
    } catch (err) {
        console.log(err);
    }
};
