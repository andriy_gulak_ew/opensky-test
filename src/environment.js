export const BASE_URL = 'https://opensky-network.org/api';
export const DEPARTURE_FLIGHTS = BASE_URL + '/flights/departure'; //?airport=EHAM&begin=1517221200&end=1517229000
export const ARRIVAL_FLIGHTS = BASE_URL + '/flights/arrival'; //?airport=EHAM&begin=1517221200&end=1517229000
